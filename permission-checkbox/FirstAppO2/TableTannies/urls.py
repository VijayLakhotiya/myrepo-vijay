from django.contrib import admin
from django.urls import path, include
from Tournament import views
from django.views.generic.base import TemplateView # new

urlpatterns = [

    path('Test/', include('Test.urls')),  # new
    path('Tournament/', include('Tournament.urls')),  # new
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
]

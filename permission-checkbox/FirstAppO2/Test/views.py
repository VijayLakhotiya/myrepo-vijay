from .models import Employee, Emp
from .forms import EmpForm, EmpForm2 # , EmpForm3

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from bootstrap_datepicker_plus import DateTimePickerInput, TimePickerInput


class CreateView(generic.edit.CreateView):
    model = Emp
    fields = '__all__'
    template_name = 'emp_form.html'
    def get_form(self):
        form = super().get_form()
        form.fields['completeDateTime'].widget = DateTimePickerInput()
        return form


def home(request):
    if request.method == 'POST':
        form = EmpForm(request.POST)
        # form = EmpForm2(request.POST)
        print("$$$$$$$", request.POST)

        if form.is_valid():
            print("^^^^^^^",request.POST)
            form.save()
    else:
        form = EmpForm()
        form.fields['completeDateTime'].widget = TimePickerInput()
        # form = EmpForm2()

    return render(request, 'emp_form.html', {'form': form})


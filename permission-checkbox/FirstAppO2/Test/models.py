from django.db import models
from django.utils import timezone

# Create your models here.


class Employee(models.Model):
    emp_name = models.CharField(default='a_', max_length=20)
    emp_tag = models.CharField(max_length=200, default='', null = True)
    completeDateTime = models.DateTimeField(default='2019-10-11 09:00', blank=True, null=True)

    def __str__(self):
        return self.emp_name

class Emp(models.Model):
    emp_n = models.CharField(default='a_', max_length=20)
    completeDateTime = models.DateField( blank=True, null=True)

    def __str__(self):
        return self.emp_n


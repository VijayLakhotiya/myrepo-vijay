from django.forms import *      # ModelForm, DateTimeField, DateInput, DateTimeInput,
from django.contrib.admin import widgets

# from .widgets.select_time_widget import * # See __all__.
from .models import Emp
from bootstrap_datepicker_plus import TimePickerInput, DateTimePickerInput
from django_bootstrap_datetimepicker.widgets import BootstrapDateTimeInput
from tempus_dominus.widgets import DatePicker, TimePicker, DateTimePicker
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget


# class EmpForm3(ModelForm):
#     time2 = TimeField(widget=SelectTimeWidget())
#
#     class Meta:
#         model = Emp
#         fields = '__all__'
    # time = TimeField(widget=SelectTimeWidget(twelve_hr=True, use_seconds=False, required=False), required=False, label=u'Time')


class EmpForm2(ModelForm):
    class Meta:
        model = Emp
        fields = '__all__'

        widgets = {
            'start_time': TimePickerInput(),
            'start_datetime': DateTimePickerInput(),
        }


class EmpForm(ModelForm):
    # timeonly = TimeField(input_formats=('%I:%M %p',...,...))
    date = DateTimeField(
        widget=DateTimeWidget()
    )

    timeonly = TimeField(
        widget=TimeWidget()
    )


    class Meta:
        model = Emp
        fields = '__all__'


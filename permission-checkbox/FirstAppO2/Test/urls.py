from django.urls import path, include
from django.conf.urls import url
from . import views


urlpatterns = [
    path('', views.home),
    path('create/', views.CreateView.as_view(), name='create'),

]


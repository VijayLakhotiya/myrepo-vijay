from django import forms
from .models import Team, Player
from django.forms.models import inlineformset_factory

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from .custom_layout_object import *


class AddTeamForm(forms.ModelForm):

    class Meta:
        model = Team
        exclude = ['created_by', ]

    def __init__(self, *args, **kwargs):
        super(AddTeamForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3 create-label'
        self.helper.field_class = 'col-md-9'
        self.helper.layout = Layout(
            'team_name',
            'tagline',
            # 'banner_name',
            'team_banner',
            HTML("""{% if form.team_banner.value %}
                           <img height="80"
                                width="160"
                                class="pull-left"
                                src="/media/{{ form.team_banner }}">
                         {% endif %}""", ),
            Fieldset('Add titles',
                Formset('titles')),
            # Field('note'),
            HTML("<br>"),
            ButtonHolder(Submit('submit', 'Save')),
        )


class AddPlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = [
            'players_name',
            'players_gender',
            'players_tagline',
            'players_team',
            'is_captain',
        ]


AddPlayerFormSet = inlineformset_factory(Team, Player,form=AddPlayerForm, extra=1, can_delete=True)

from django.urls import path, include
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required, permission_required


urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),

    path('admin/auth/group/adds/', views.Otoo, name='otto'),

    path('create_team_players/', views.TeamCreate.as_view(), name='team_create'),

    path('add_data/', views.AddDataViews.as_view(), name='add_data'),
    path('create_team_data/', views.add_team, name='add_team'),
    path('create_player_data/', views.add_player, name='add_player'),

    path('accounts/', include('allauth.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)



from django.contrib import admin
from .models import Team, Team_Match, Player, Result, Scheduled_Match, Set
from import_export.admin import ImportExportModelAdmin
from import_export import resources

from django.contrib.auth.admin import GroupAdmin

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
# from .views import Otoo

# Register your models here.
@admin.register(Team, Team_Match, Player, Result, Scheduled_Match, Set)
class ViewAdmin(ImportExportModelAdmin):
    pass


admin.site.unregister(Group)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    change_form_template = 'otto/checkbox_permission.html'
    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ('permissions',)

    def get_app_models(self):
        extra_context = {}
        all_apps = list(ContentType.objects.values_list('id', 'app_label'))
        for app in all_apps:
            all_content_type = ContentType.objects.values_list('id', 'model')
            app_name = app[1]
            models_dic = {}
            models = all_content_type.filter(app_label=app[1])

            for model in models:
                model_permission = Permission.objects.values_list('id', 'codename')
                model_name = model[1]
                model_perm_list = []
                all_perm = model_permission.filter(content_type_id=model[0])

                for perm in all_perm:
                    perm_id_dic = {}
                    perm_id_dic[perm[0]] = perm[1]
                    model_perm_list.append(perm_id_dic)

                models_dic[model_name] = model_perm_list

            extra_context[app_name] = models_dic
        return extra_context

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = self.get_app_models()
        if request.POST:
            group_name = request.POST.get('group_name')
            user_permissions = request.POST.getlist('permission_checkboxes[]')
            name = Group.objects.update_or_create(name=group_name)  # add Group name in auth_group model
            p = Group.objects.get(name=group_name)
            p.permissions.set(user_permissions)

        check_perm = Group.objects.get(id = object_id)
        have_perms = check_perm.permissions.all()
        have_perm_list = []

        for id in have_perms:
            have_perm_list.append(id.id)
        have_perm_dict = {}
        have_perm_dict['have_perm_list'] = have_perm_list
        have_perm_dict['name'] = check_perm.name

        return self.changeform_view(request, object_id, form_url, {'extra_context' : extra_context, 'have_perm_dict': have_perm_dict})

    def add_view(self, request, form_url=''):
        extra_context = self.get_app_models()
        if request.POST:
            group_name = request.POST.get('group_name')
            user_permissions = request.POST.getlist('permission_checkboxes[]')
            name = Group.objects.update_or_create(name=group_name)  # add Group name in auth_group model
            p = Group.objects.get(name=group_name)
            p.permissions.set(user_permissions)

        return self.changeform_view(request, None, form_url, {'extra_context' : extra_context})

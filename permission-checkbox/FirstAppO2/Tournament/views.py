from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView
from .forms import *    # AddTeamForm, AddPlayerForm, AddPlayerFormSet
from .models import *   # Team, Player
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db import transaction, IntegrityError
from django.forms import modelformset_factory
from django.contrib.auth.models import User, Group
# from users.models import CustomUser
from django.template import loader
from django.http import HttpResponse
from tablib import Dataset

# from Test.otto import *
# from .newuser import *
from TableTannies.settings import *
from django.apps import apps
from django.db import models
# Create your views here.


class HomePageView(TemplateView):
    template_name = 'home.html'


class AddDataViews(TemplateView):
    template_name = 'add_data_views.html'





def Otoo(request):
    from django.contrib.auth.models import Permission, Group
    from django.contrib.contenttypes.models import ContentType
    perm_dic = {}
    object_id = 8
    all_apps = set(ContentType.objects.values_list('id', 'app_label'))
    if request.POST:
        group_name = request.POST.get('group_name')
        user_permissions = request.POST.getlist('Permission_checkboxes[]')

        name = Group.objects.update_or_create(name=group_name)            # add Group name in auth_group model
        p = Group.objects.get(name=group_name)
        p.permissions.set(user_permissions)

    for app in all_apps:
        all_content_type = ContentType.objects.values_list('id','model')
        app_name = app[1]
        models_dic = {}
        models = all_content_type.filter(app_label=app[1])

        for model in models:
            model_permission = Permission.objects.values_list('id','codename')
            model_name = model[1]
            model_perm_list = []
            all_perm = model_permission.filter(content_type_id = model[0])

            for perm in all_perm:
                perm_id_dic = {}
                perm_id_dic[perm[0]] = perm[1]
                model_perm_list.append(perm_id_dic)

            models_dic[model_name] = model_perm_list

        perm_dic[app_name] = models_dic

    print('\n\n',perm_dic)

    return render(request, "otto/otto_checkbox_permission.html", {'extra_context': perm_dic} )




class TeamCreate(LoginRequiredMixin, CreateView):
    model = Team
    template_name = 'mycollections/team_create.html'
    form_class = AddTeamForm
    success_url = None

    def get_context_data(self, *args, **kwargs):
        data = super(TeamCreate, self).get_context_data(**kwargs)

        if self.request.POST:
            data['titles'] = AddPlayerFormSet(self.request.POST, self.request.FILES)
            postteam = Team()
            print(self.request.POST)
            print("&&&&&&&&")
            postteam.team_name = self.request.POST.get('team_name')
            postteam.tagline = self.request.POST.get('tagline')
            # postteam.tagline = self.request.POST.get('banner_name')
            postteam.team_banner = self.request.POST.get('team_banner')

            postteam.created_by = self.request.user
            postteam.updated_by = self.request.user
            postteam.save()
            Countplayers = 1
            print('^^^^^^^^')
            print(self.request.POST.get('team_banner'))
            for i in range(0, Countplayers):

                print("Now players")
                postplayer = Player()
                postplayer.players_name = self.request.POST.get('Players_Team-'+str(i)+'-players_name')
                postplayer.players_gender = self.request.POST.get('Players_Team-'+str(i)+'-players_gender')
                postplayer.players_tagline = self.request.POST.get('Players_Team-'+str(i)+'-players_tagline')
                postplayer.is_captain = bool(self.request.POST.get('is_captain'))  # self.request.POST.get('id_Players_Team-0-is_captain')

                team_obj = Team.objects.get(team_name = self.request.POST.get('team_name'))
                postplayer.players_team = team_obj

                postplayer.created_by = self.request.user
                postplayer.updated_by = self.request.user
                postplayer.save()

        else:
            data['titles'] = AddPlayerFormSet()
        return data

    def get_success_url(self):
        return reverse_lazy('mycollections:collection_detail', kwargs={'pk': self.object.pk})


@login_required(login_url='/accounts/login/')
def add_team(request):
    team = AddTeamForm()
    if request.method == 'POST':
        if request.POST.get('team_name') and request.POST.get('team_banner') and request.POST.get('team_banner'):
            post = Team()
            post.team_name = request.POST.get('team_name')
            post.team_banner = request.POST.get('team_banner')
            post.tagline = request.POST.get('tagline')
            post.created_by = request.user
            post.updated_by = request.user

            post.save()
            return render(request, "AddData/add_team.html", {'form': team})

    else:
        team = AddTeamForm()
    return render(request,"AddData/add_team.html",{'form':team})


@login_required(login_url='/accounts/login/')
def add_player(request):
    player = AddPlayerForm()
    if request.method == 'POST':
        if request.POST.get('players_name') and request.POST.get('players_gender') and request.POST.get(
                'players_tagline') and request.POST.get('players_team'):
            post = Player()
            post.players_name = request.POST.get('players_name')
            post.players_gender = request.POST.get('players_gender')
            post.players_tagline = request.POST.get('players_tagline')

            team_obj = Team.objects.get(id=request.POST.get("players_team"))
            post.players_team = team_obj

            post.is_captain = bool(request.POST.get('is_captain'))
            post.created_by = request.user
            post.updated_by = request.user
            post.save()
            return render(request, "AddData/add_player.html", {'form': player})

    else:
        player = AddPlayerForm()
    return render(request,"AddData/add_player.html",{'form':player})




from django.db import models
from django.contrib.auth.models import User
# from users.models import CustomUser
from django_currentuser.middleware import ( get_current_user, get_current_authenticated_user )
from django_currentuser.db.models import CurrentUserField

# Create your models here.

GENDER = (
    ('null', 'Deny to disclosed'),
    ('male', 'MALE'),
    ('female', 'FEMALE'),
    ('other', 'OTHER'),
)
RESULT = (
    ('null', ''),
    ('won', 'WON'),
    ('lose', 'LOSE'),
)
MATCH_TYPE = (
    ('singles', 'SINGLES'),
    ('doubles', 'DOUBLES')
)


class Team(models.Model):
    # team_id = models.IntegerField(primary_key = True)
    team_name = models.CharField(max_length=20)
    banner_name = models.CharField(max_length=200, default='', null = True)
    team_banner = models.FileField(upload_to='media/', max_length=255, default = '', blank=True)
    tagline = models.CharField(max_length=250, null=True)
    created_by = models.ForeignKey(User, related_name='Team_Created_By', on_delete=models.DO_NOTHING)
    updated_by = models.ForeignKey(User, related_name='Team_Updated_By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.team_name


class Player(models.Model):
    # players_id = models.IntegerField(primary_key = True)
    players_name = models.CharField(max_length=20, default = '')
    players_gender = models.CharField(max_length=20, choices = GENDER, default='')
    players_team = models.ForeignKey(Team, related_name='Players_Team',on_delete=models.DO_NOTHING)
    players_tagline = models.CharField(max_length=50)
    is_captain = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='Player_Created_By', on_delete=models.DO_NOTHING)
    updated_by = models.ForeignKey(User, related_name='Player_Updated_By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.players_name


class Team_Match(models.Model):
    # teams_match_id = models.IntegerField(primary_key = True)
    date_time = models.DateTimeField(auto_now=False)
    team_1 = models.ForeignKey(Team,related_name='team_1',on_delete=models.DO_NOTHING)
    team_2 = models.ForeignKey(Team,related_name='team_2',on_delete=models.DO_NOTHING)
    match_result = models.CharField(max_length=10, choices=RESULT)
    created_by = models.ForeignKey(User, related_name='Team_Match_Created_By', on_delete=models.DO_NOTHING)
    updated_by = models.ForeignKey(User, related_name='Team_Match_Updated_By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.Date_Time


class Result(models.Model):
    # result_id = models.IntegerField(primary_key = True)
    teams_match = models.ForeignKey(Team_Match,on_delete=models.DO_NOTHING)
    final_result = models.CharField(max_length=10, choices=RESULT)
    created_by = models.ForeignKey(User, related_name='Result_Created_By', on_delete=models.DO_NOTHING)
    updated_by = models.ForeignKey(User, related_name='Result_Updated_By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.Result_ID


class Scheduled_Match(models.Model):
    # match_id = models.IntegerField(primary_key = True)
    teams_match = models.ForeignKey(Team_Match,on_delete=models.DO_NOTHING)
    match_type = models.CharField(max_length=10, choices=MATCH_TYPE)
    player1_team1 = models.ForeignKey(Player, related_name='player1_team1', null=True,on_delete=models.DO_NOTHING)
    player2_team1 = models.ForeignKey(Player, related_name='player2_team1', null=True,on_delete=models.DO_NOTHING)
    player1_team2 = models.ForeignKey(Player, related_name='player1_team2', null=True,on_delete=models.DO_NOTHING)
    player2_team2 = models.ForeignKey(Player, related_name='player2_team2', null=True,on_delete=models.DO_NOTHING)
    team1_wonrate =  models.FloatField()
    team2_wonrate = models.FloatField()
    umpire = models.CharField(max_length=20)
    assistant = models.CharField(max_length=20)
    match_result = models.CharField(max_length=10, choices=RESULT)
    manofthematch = models.ForeignKey(Player, related_name='manofthematch',null=True,on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(User, related_name='Scheduled_Match_Created_By', on_delete=models.DO_NOTHING)
    updated_by = models.ForeignKey(User, related_name='Scheduled_Match_Updated_By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.Match_ID


class Set(models.Model):
    # set_id = models.IntegerField(primary_key = True)
    scheduled_match_id = models.ForeignKey(Scheduled_Match,on_delete=models.DO_NOTHING)
    team1_score = models.IntegerField()
    team2_score = models.IntegerField()
    set_result = models.IntegerField()
    created_by = models.ForeignKey(User, related_name='Set_Created_By', on_delete=models.DO_NOTHING)
    updated_by = models.ForeignKey(User, related_name='Set_Updated_By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.Set_ID


# Generated by Django 2.2.4 on 2019-08-27 11:40

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('players_name', models.CharField(default='', max_length=20)),
                ('players_gender', models.CharField(choices=[('null', 'Deny to disclosed'), ('male', 'MALE'), ('female', 'FEMALE'), ('other', 'OTHER')], default='', max_length=20)),
                ('players_tagline', models.CharField(max_length=50)),
                ('is_captain', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Player_Created_By', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Scheduled_Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('match_type', models.CharField(choices=[('singles', 'SINGLES'), ('doubles', 'DOUBLES')], max_length=10)),
                ('team1_wonrate', models.FloatField()),
                ('team2_wonrate', models.FloatField()),
                ('umpire', models.CharField(max_length=20)),
                ('assistant', models.CharField(max_length=20)),
                ('match_result', models.CharField(choices=[('null', ''), ('won', 'WON'), ('lose', 'LOSE')], max_length=10)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Scheduled_Match_Created_By', to=settings.AUTH_USER_MODEL)),
                ('manofthematch', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='manofthematch', to='Tournament.Player')),
                ('player1_team1', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='player1_team1', to='Tournament.Player')),
                ('player1_team2', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='player1_team2', to='Tournament.Player')),
                ('player2_team1', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='player2_team1', to='Tournament.Player')),
                ('player2_team2', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='player2_team2', to='Tournament.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('team_name', models.CharField(max_length=20)),
                ('banner_name', models.CharField(default='', max_length=200, null=True)),
                ('team_banner', models.FileField(blank=True, default='', max_length=255, upload_to='media/')),
                ('tagline', models.CharField(max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Team_Created_By', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Team_Updated_By', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Team_Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateTimeField()),
                ('match_result', models.CharField(choices=[('null', ''), ('won', 'WON'), ('lose', 'LOSE')], max_length=10)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Team_Match_Created_By', to=settings.AUTH_USER_MODEL)),
                ('team_1', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='team_1', to='Tournament.Team')),
                ('team_2', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='team_2', to='Tournament.Team')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Team_Match_Updated_By', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Set',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('team1_score', models.IntegerField()),
                ('team2_score', models.IntegerField()),
                ('set_result', models.IntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Set_Created_By', to=settings.AUTH_USER_MODEL)),
                ('scheduled_match_id', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Tournament.Scheduled_Match')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Set_Updated_By', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='scheduled_match',
            name='teams_match',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Tournament.Team_Match'),
        ),
        migrations.AddField(
            model_name='scheduled_match',
            name='updated_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Scheduled_Match_Updated_By', to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('final_result', models.CharField(choices=[('null', ''), ('won', 'WON'), ('lose', 'LOSE')], max_length=10)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Result_Created_By', to=settings.AUTH_USER_MODEL)),
                ('teams_match', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Tournament.Team_Match')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Result_Updated_By', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='player',
            name='players_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Players_Team', to='Tournament.Team'),
        ),
        migrations.AddField(
            model_name='player',
            name='updated_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='Player_Updated_By', to=settings.AUTH_USER_MODEL),
        ),
    ]
